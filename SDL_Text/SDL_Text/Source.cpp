#include <SDL.h>
#include <SDL_ttf.h>
#include <string>
#include <sstream> 
#include <iostream>
using namespace std;

int main(int argc, char ** argv)
{
	bool quit = false;	// bool for quitting
	SDL_Event event;

	SDL_Init(SDL_INIT_VIDEO);
	TTF_Init();		//initializing SDL text
	
	//creating a window
	SDL_Window * window = SDL_CreateWindow("SDL_ttf in SDL2",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640,
		480, 0);

	//creating a renderer for the window
	SDL_Renderer * renderer = SDL_CreateRenderer(window, -1, 0);

	//creating font with  arguements (font file, font size)
	TTF_Font * font = TTF_OpenFont("arial.ttf", 25);
	const char * error = TTF_GetError();

	//creating a string stream
	std::stringstream strm;
	int i = 12345;
	strm << i;

	//creating a colour
	SDL_Color color = { 255, 255, 255 };

	//creating a surface for the font using the font and colour defined above
	SDL_Surface * surface = TTF_RenderText_Solid(font, strm.str().c_str(), color);

	//creating a texture using the renderer and surface with the text
	SDL_Texture * texture = SDL_CreateTextureFromSurface(renderer, surface);

	int texW = 0;
	int texH = 0;
	SDL_QueryTexture(texture, NULL, NULL, &texW, &texH);
	SDL_Rect dstrect = { 100, 100, texW*2, texH*2 };

	while (!quit)
	{
		SDL_WaitEvent(&event);

		switch (event.type)
		{
		case SDL_QUIT:
			quit = true;
			break;
		}

		SDL_RenderCopy(renderer, texture, NULL, &dstrect);
		SDL_RenderPresent(renderer);
	}

	//clean up
	SDL_DestroyTexture(texture);
	SDL_FreeSurface(surface);
	TTF_CloseFont(font);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	TTF_Quit();
	SDL_Quit();

	return 0;
}