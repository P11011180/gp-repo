/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		Game.h
*
************************************************/
#pragma once
#ifndef GAME_H
#define GAME_H

#include "Player.h"
#include "Goblin.h"
#include "Orc.h"
#include "Troll.h"
#include "Skeleton.h"
#include <vector>
#include <algorithm>
#include <time.h>
#include <stdlib.h> 
#include "DisplayMap.h"
#include <iostream>
#include <fstream>
#include "SoundManager.h"
#include "Item.h"

class Game
{
public:
	Game();
	~Game();

	void init();	//functions for the game loop
	void draw(); 
	void update();
	void battle();
	void info();
	void clean();

	int getKillCount();
	bool getRunning();
	void setRunning(bool running);
	std::pair<int, int> getRandomCoords();	//generates random coordinates
	void fileHandler(int newKills);			//deals with file i/o
	void openingStory();

protected:
	
	std::vector <GameObject*> vpGameObjs;	//vector for the game object pointers
	std::vector <Item*> vpItems;	//vector for the Item pointers
	bool m_running;		// check to see if the game isnt over
	int m_enemyKillCount;	//how many enemies you have killed
	int m_enemySelector;	//used to randomly select enemy type
	DisplayMap* map = new DisplayMap();	//pointer for displaying the map

	int m_playerBattleMove;
	int m_enemyBattleMove;
	std::string m_advance; // take input from the player to advance through the game
};
#endif
