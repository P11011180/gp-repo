/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		Item.cpp
*
************************************************/
#include "Item.h"

Item::Item(std::string typeID, int x, int y, char character)
{
	m_typeID = typeID;
	m_x = x;
	m_y = y;
	m_character = character;
	m_active = true;
}

Item::~Item()
{

}

std::pair<int, int> Item::getCoords()
{
	return std::make_pair(m_x, m_y);
}

char Item::getCharacter()
{
	return m_character;
}

std::string Item::getType()
{
	return m_typeID;
}

int Item::getX()
{
	return m_x;
}

int Item::getY()
{
	return m_y;
}

void Item::pickedUp()
{
	m_character = ' ';
	m_active = false;
}

bool Item::getActive()
{
	return m_active;
}
