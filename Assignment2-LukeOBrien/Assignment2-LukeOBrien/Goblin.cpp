/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		Goblin.cpp
*
************************************************/
#include "Goblin.h"

int Goblin::battleTurn()
{
	enemyRandomAttack = rand() % 3;

	if (enemyRandomAttack == 0)
	{
		std::cout << "Goblin: 'hahaha try this on for size'\n";
		std::cout << "The " << m_typeID << " threw a Fire Bomb'\n";
		return m_attack;
	}
	else if (enemyRandomAttack == 1)
	{
		std::cout << "Goblin: 'I'll stick you full of holes.'\n";
		std::cout << "The " << m_typeID << " waves a small rusty dagger around frantically.\n";
		return (m_attack * 1.2);
	}
	else if (enemyRandomAttack == 2)
	{
		std::cout << "Goblin: 'Don't inturrupt me while I'm GOBLIN up my food'\n";
		std::cout << "The " << m_typeID << " ate some food and regained 30 health\n\n";
		m_health += 30;
		return 0;
	}
}