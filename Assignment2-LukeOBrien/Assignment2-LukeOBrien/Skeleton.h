#pragma once
/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		Skeleton.h
*
************************************************/
#pragma once
#ifndef Skeleton_H
#define Skeleton_H

#include "Enemy.h"

class Skeleton : public Enemy
{
public:
	virtual int battleTurn();
protected:

};
#endif