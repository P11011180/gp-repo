/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		Player.cpp
*
************************************************/
#include "Player.h"

Player::Player()
{
	std::cout << "PLAYER CONSTRUCTER CALLED\n";
}

Player::~Player()
{
	std::cout << "PLAYER DECONSTRUCTER CALLED\n";
}

void Player::update()
{
	std::cout << "Please enter which direction you want to move in: \n";	// ask the user to select a direction
	std::cout << "('w' = UP) ('a' = LEFT) ('s' = DOWN) ('d' = RIGHT) \n";
	std::cin >> m_moveDirection;

	if (!(m_moveDirection == "w" || m_moveDirection == "a" || m_moveDirection == "s" || m_moveDirection == "d"))	//check for valid input. if not valid then the player misses a turn
	{
		std::cout << "You have entered an incorrect input and have lost your turn\n";
	}
	else
	{
		movePlayer();
	}
}


void Player::movePlayer() // function to move the player
{
	if (m_moveDirection == "w") // performs boundary checks for moving UP
	{
		if ((m_y - m_speed) >= 0)	//if the player doesnt collide with the wall
		{
			m_y -= m_speed;	// move player
		}
		else
		{
			m_y = 0;	//hit wall
		}
	}
	

	if (m_moveDirection == "a")// performs boundary checks for moving LEFT
	{
		if ((m_x - m_speed) >= 0)
		{
			m_x -= m_speed;
		}
		else
		{
			m_x = 0;
		}
	}
	
	if (m_moveDirection == "s") // performs boundary checks for moving DOWN
	{
		if ((m_y + m_speed) <= 9)
		{
			m_y += m_speed;
		}
		else
		{
			m_y = 9;
		}
	}
	
	if (m_moveDirection == "d") // performs boundary checks for moving RIGHT
	{
		if ((m_x + m_speed) <= 9)
		{
			m_x += m_speed;
		}
		else
		{
			m_y = 9;
		}
	}
	
}

void Player::levelUp()
{
	m_health += 30;
	m_defence += 5;
	m_attack += 10;
	m_level += 1;
	std::cout << "\nYou have leveled up!\n";
	if (m_level == 2)
	{
		std::cout << "You have learned a new spell!\n";
	}
	std::cout << "\nEnter any key to continue!\n";
	std::cin >> m_wait;
}


int Player::battleTurn()
{
	std::cout << "\nChoose your action wisely, your life is on the line.\n";
	std::cout << "1. FireBall \n2. Icicle Spear \n3. Heal \n";
	if (m_level > 1)
	{
		std::cout << "4. Final Flash \n";
	}
	std::cin >> m_attackChoice;
	TheSoundManager::Instance()->playSound("selection", 1);
	system("CLS");
	if (m_attackChoice == 1)
	{
		std::cout << "You make a motion as if you are throwing a ball and fire starts forming in your hand'\n";
		std::cout << "The " << m_typeID << " threw a FireBall'\n";
		return m_attack;
	}
	else if (m_attackChoice == 2)
	{
		std::cout << "You start whispering an incantation under your breath.\n";
		std::cout << "An Icicle shoots up from beneath the enemy, impaling them.\n";
		return (m_attack * 1.5);
	}
	else if (m_attackChoice == 3)
	{
		std::cout << "You close your eyes as you are enveloped in an aura of a warm magical glow\n";
		std::cout << "The " << m_typeID << " recovers her health\n\n";
		m_health += 50;
		return 0;
	}
	else if (m_attackChoice == 4)
	{
		std::cout << "You raise your hands in the air as an intense heat starts to form in them\n";
		std::cout << "You then hurl a giant ball of energy towards the enemy\n";
		return (m_attack * 1.7);
	}
}