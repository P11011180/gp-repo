/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		GameObject.cpp
*
************************************************/
#include "GameObject.h"

GameObject::GameObject()
{
	std::cout << "GAME OBJECT CONSTRUCTOR CALLED" << std::endl;
}
GameObject::~GameObject()
{
	std::cout << "GAME OBJECT DECONSTRUCTOR CALLED" << std::endl;
}

void GameObject::spawn(std::string typeID, int health, int speed, int attack, int defence, int x, int y, char character, int level)
{
	m_typeID = typeID;
	m_health = health;
	m_speed = speed;
	m_attack = attack;
	m_defence = defence;
	m_x = x;
	m_y = y;
	m_character = character;
	m_level = level;
}


void GameObject::draw()
{
	std::cout << "TypeID:" << m_typeID << "\t\t\t (x,y) coords = ( " << m_x << ", " << m_y << " )\n";
}


void GameObject::update()
{
	// its free real estate
}


void GameObject::info()
{
	std::cout << "TypeID = " << m_typeID;
	std::cout << "\tLevel = " << m_level;
	std::cout << "\tHealth = " << m_health;
	std::cout << "\tSpeed = " << m_speed;
	std::cout << "\tAttack = " << m_attack;
	std::cout << "\tDefence = " << m_defence;
	std::cout << "\tcoords = " << "(" << m_x << ", " << m_y << ")" << std::endl;
}


bool GameObject::isAlive()
{
	if (m_health > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

int GameObject::getX()	//just a bunch of getters 
{
	return m_x;
}

int GameObject::getY()
{
	return m_y;
}

std::string GameObject::getType()
{
	return m_typeID;
}

int GameObject::getHealth()
{
	return m_health;
}

void GameObject::setHealth(int attack)
{
	if (attack > m_defence)
	{
		attack = attack - m_defence;
		std::cout << "\nThe " << getType() << " takes " << attack << " damage.\n\n";
		m_health -= attack;
	}
}

int GameObject::battleTurn()
{
	//virtual
	return 0;
}

void GameObject::levelUp()
{
	//virtual
}


char GameObject::getCharacter()
{
	return m_character;
}


std::pair<int, int> GameObject::getCoords()
{
	return std::make_pair(m_x, m_y);
}
