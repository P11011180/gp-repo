/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		Orc.cpp
*
************************************************/
#include "Orc.h"

int Orc::battleTurn()
{
	enemyRandomAttack = rand() % 3;

	if (enemyRandomAttack == 0)
	{
		std::cout << "Orc: 'I'll cut open your gut.'\n";
		std::cout << "The " << m_typeID << " used Slash\n";
		return m_attack;
	}
	else if (enemyRandomAttack == 1)
	{
		std::cout << "Orc: 'I'll make you wish your were dead.'\n";
		std::cout << "The " << m_typeID << " used Headbutt\\n";
		return (m_attack * 1.5);
	}
	else if (enemyRandomAttack == 2)
	{
		std::cout << "Orc: 'This is the end for you.'\n";
		std::cout << "The " << m_typeID << " used 'War Cry'. It doubles his attack at the cost of its defence\n\n";
		m_attack += m_attack;
		m_defence = 0;
		return 0;
	}
}
