#pragma once
/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		Troll.h
*
************************************************/
#pragma once
#ifndef TROLL_H
#define TROLL_H

#include "Enemy.h"

class Troll : public Enemy
{
public:
	virtual int battleTurn();
protected:

};
#endif