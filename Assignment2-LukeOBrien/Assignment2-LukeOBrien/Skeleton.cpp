/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		Skeleton.cpp
*
************************************************/
#include "Skeleton.h"

int Skeleton::battleTurn()
{
	enemyRandomAttack = rand() % 3;

	if (enemyRandomAttack == 0)
	{
		std::cout << "The skeleton rips off its own arm and throws it at you.\n";
		std::cout << "The " << m_typeID << " used BONEmerang\n";
		return m_attack;
	}
	else if (enemyRandomAttack == 1)
	{
		std::cout << "Skeleton: 'You really get under my skin.... although I don't have any.'.\n";
		std::cout << "The " << m_typeID << " used Bone Whack\n";
		return (m_attack * 1.3);
	}
	else if (enemyRandomAttack == 2)
	{
		std::cout << "Skeleton: 'I need some calcium or I'm BONED'.\n";
		std::cout << "The " << m_typeID << " drinks milk and regained 30 health\n\n";
		m_health += 30;
		return 0;
	}
}