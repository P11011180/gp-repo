/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		Goblin.h
*
************************************************/
#pragma once
#ifndef GOBLIN_H
#define GOBLIN_H

#include "Enemy.h"

class Goblin : public Enemy
{
public:
	virtual int battleTurn();

protected:

};
#endif