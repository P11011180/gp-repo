#pragma once
/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		Item.h
*
************************************************/
#ifndef ITEM
#define ITEM
#include <string>
class Item
{
public:
	Item(std::string typeID, int x, int y, char character);	//item constructor
	~Item();
	std::pair <int, int> getCoords();	// for getting the coordinates
	char getCharacter();		//getting the map icon
	std::string getType();		//the type
	int getX();	//get x coord
	int getY(); //get y coord
	void pickedUp();	//function to allow items to only be picked up once
	bool getActive();	// bool of whether the item is active or not

protected:
	int m_x;
	int m_y;
	char m_character;
	bool m_active;
	std::string m_typeID;

};


#endif
