/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		Player.h
*
************************************************/



#pragma once
#ifndef PLAYER_H //defining player
#define PLAYER_H

#include "GameObject.h"

class Player : public GameObject // player is a gameobject
{
public:
	Player();	
	~Player();
	virtual void update();
	void movePlayer();	//function to deal with player movement
	void levelUp();
	virtual int battleTurn();
protected:
	int m_attackChoice;
	std::string m_moveDirection;	//to store the user selected move direction

};
#endif

