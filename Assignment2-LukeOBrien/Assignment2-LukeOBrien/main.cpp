// a program, a game, a programmed game
/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		main.cpp
*
************************************************/

#include "Game.h"
#include <time.h>
int main()
{
	srand(time(NULL));	//to seed the random coords

	Game* game = new Game();	// game pointer
	game->init();	// initial setup of the game

	while ((game->getRunning() == true))	//while player is still alive
	{
		game->draw();
		game->update();
		game->battle();
		game->info();
		game->clean();
	}

	game->info();

	std::cout << "\The game is over!" << std::endl;

	game->fileHandler(game->getKillCount());	//calls the file handler

	system("pause");
	return 0;
}
