#pragma once
/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		Orc.h
*
************************************************/
#pragma once
#ifndef ORC_H
#define ORC_H

#include "Enemy.h"

class Orc : public Enemy
{
public:
	virtual int battleTurn();
protected:

};
#endif