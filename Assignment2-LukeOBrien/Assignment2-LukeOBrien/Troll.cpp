/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		Troll.cpp
*
************************************************/
#include "Troll.h"

int Troll::battleTurn()
{
	enemyRandomAttack = rand() % 3;

	if (enemyRandomAttack == 0)
	{
		std::cout << "Troll: 'RRRARRRRGGGHHH'.\n";
		std::cout << "The " << m_typeID << " used Smash\n";
		return m_attack;
	}
	else if (enemyRandomAttack == 1)
	{
		std::cout << "Troll: 'I will squash you, puny human'.\n";
		std::cout << "The " << m_typeID << " used Pummel\n";
		return (m_attack * 1.1);
	}
	else if (enemyRandomAttack == 2)
	{
		std::cout << "Troll: 'ZzZzZzZzZ'.\n";
		std::cout << "The " << m_typeID << " is Resting and regained 20 health\n\n";
		m_health += 20;
		return 0;
	}
}