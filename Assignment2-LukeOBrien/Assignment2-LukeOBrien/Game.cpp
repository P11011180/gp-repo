/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		Game.cpp
*
************************************************/
#include "Game.h"

Game::Game()
{
	m_running = true;
	DisplayMap* map = new DisplayMap();
	m_enemyKillCount = 0;
	
}

Game::~Game()
{

}


void Game::init()
{
	TheSoundManager::Instance()->load("assets/OnikuLoop.wav", "music1", SOUND_MUSIC);  //loading the music
	TheSoundManager::Instance()->load("assets/selection.wav", "selection", SOUND_SFX);		//loading sound effects
	TheSoundManager::Instance()->load("assets/enemykilled.wav", "killed", SOUND_SFX);

	TheSoundManager::Instance()->playMusic("music1", -1);	// playing and looping the music

	openingStory();	//prints the opening story text
	system("CLS");

	Player* player = new Player();		//creating the player and pushing them onto the vector
	player->spawn("Player", 160, 1, 30, 10, getRandomCoords().first, getRandomCoords().second, 'P', 1);
	vpGameObjs.push_back(player);

	Item* item = new Item("Item", getRandomCoords().first, getRandomCoords().second, '+'); //creating an Item that gives the player a level up when picked up
	vpItems.push_back(item);	//pushing the item onto the item vector

	//using a loop to create enemies
	for (int i = 1; i < 5; i++)
	{
		m_enemySelector = rand() % 4;

		if (m_enemySelector == 0)
		{
			Goblin* goblin = new Goblin();
			goblin->spawn("Goblin", 80, 1, 50, 3, getRandomCoords().first, getRandomCoords().second, 'G', i, 3);	//spawning a goblin
			vpGameObjs.push_back(goblin);
		}
		if (m_enemySelector == 1)
		{
			Orc* orc = new Orc();
			orc->spawn("Orc", 130, 1, 30, 5, getRandomCoords().first, getRandomCoords().second, 'O', i, i);			//spawning an Orc
			vpGameObjs.push_back(orc);
		}
		if (m_enemySelector == 2)
		{
			Troll* troll = new Troll();
			troll->spawn("Troll", 200, 1, 30, 7, getRandomCoords().first, getRandomCoords().second, 'T', i, 6);		//spawning a Troll
			vpGameObjs.push_back(troll);
		}
		if (m_enemySelector == 3)
		{
			Skeleton* skeleton = new Skeleton();
			skeleton->spawn("Skelly", 170, 1, 50, 7, getRandomCoords().first, getRandomCoords().second, 'S', i, 7);	//spawning a skeleton
			vpGameObjs.push_back(skeleton);
		}
	}

}


void Game::draw() // cycle through the vector of Game Objects ( enemy and player objects) and call the draw function for each object
{
	std::vector <GameObject*>::const_iterator iter;
	for (iter = vpGameObjs.begin(); iter != vpGameObjs.end(); ++iter)
	{
		(*iter)->draw();
	}
}


void Game::update()	//cycle through the vector of Game Objects (enemy and player objects) and call the update function for each object
{

	std::vector <GameObject*>::const_iterator iter;	//defining a constant iterator

	map->createMap();	//creating the map

	map->mapGameObjects(vpItems.front()->getCoords(), vpItems.front()->getCharacter()); // adding the item to the map

	for (iter = vpGameObjs.begin(); iter != vpGameObjs.end(); ++iter)	//iterates through the vector and places all gameObjects in the map
	{
		map->mapGameObjects((*iter)->getCoords(), (*iter)->getCharacter());
	}	

	map->printMap();	//prints the map

	for (iter = vpGameObjs.begin(); iter != vpGameObjs.end(); ++iter)	// calls the update function of all gameObjects
	{
		(*iter)->update();
	}

}


void Game::battle()
{
	int playerX, playerY;	//player variables

	std::vector <GameObject*>::iterator iter;	//iterator for game object vector
	std::vector <Item*>::iterator itemIter;		//iterator for item vector

	playerX = vpGameObjs.front()->getX();	//get the players coords
	playerY = vpGameObjs.front()->getY();

	for (itemIter = vpItems.begin(); itemIter != vpItems.end(); ++itemIter)
	{
		if ((playerX == (*itemIter)->getX()) && (playerY == (*itemIter)->getY()) && ((*itemIter)->getActive() == true))	//if the items position is the same as the player
		{
			vpGameObjs.front()->levelUp(); // levels up the player when he picks up an item
			vpItems.front()->pickedUp();
		}
	}

	for (iter = vpGameObjs.begin(); iter != vpGameObjs.end(); ++iter)
	{
		if ((*iter)->getType() != "Player" && (playerX == (*iter)->getX()) && (playerY == (*iter)->getY()))	//if an enemy is on the same position as the player
		{

			system("CLS");	//to keep the screen tidy
			std::cout << "You have entered a battle with a deadly " << (*iter)->getType() << "\n\n";

			while (vpGameObjs.front()->getHealth() > 0 && (*iter)->getHealth() > 0)	//stays in the battle loopo until somebody dies
			{
				m_enemyBattleMove = 0;	//damage calculators for attacks
				m_playerBattleMove = 0;

				std::cout << "The Player has " << vpGameObjs.front()->getHealth() << " health." << std::endl;
				std::cout << "The " << (*iter)->getType() << " has " << (*iter)->getHealth() << " health, hang in there." << std::endl;

				m_playerBattleMove = vpGameObjs.front()->battleTurn(); //players turn
				(*iter)->setHealth(m_playerBattleMove); //effect the enemy's health

				if ((*iter)->getHealth() > 0)	//check to see if the enemy is still alive
				{
					m_enemyBattleMove = (*iter)->battleTurn(); //enemies turn
					vpGameObjs.front()->setHealth(m_enemyBattleMove); //apply the enemy's attack
				}
				if (vpGameObjs.front()->getHealth() > 0 && (*iter)->getHealth() < 1)	//checks to see if the player is alive and if the enemy is dead
				{
					TheSoundManager::Instance()->playSound("killed", 0);
					vpGameObjs.front()->levelUp();	//levels up the player
					m_enemyKillCount++;
				}
			}
			
		}
		if ((vpGameObjs.front()->getHealth() < 1) || (vpGameObjs.size() == 1))
		{
			m_running = false;
		}
		
	}
}


void Game::info()	// call info() for each gameObject
{
	std::vector <GameObject*>::const_iterator iter;
	for (iter = vpGameObjs.begin(); iter != vpGameObjs.end(); ++iter)
	{
		(*iter)->info();
	}
}


void Game::clean()	//remove any gameObjects that have zero health
{
	std::vector <GameObject*>::iterator iter;

	for (iter = vpGameObjs.begin(); iter != vpGameObjs.end(); )
	{
		if ((!(*iter)->isAlive()))
		{
			delete (*iter);
			iter = vpGameObjs.erase(iter);
		}
		else
		{
			++iter;
		}
	}
}



int Game::getKillCount()	
{
	return m_enemyKillCount;
}


bool Game::getRunning()
{
	return m_running;
}



void Game::setRunning(bool running)
{
	m_running = running;
}


std::pair<int, int> Game::getRandomCoords() //generate random coords and return them as a pair
{
	
	int x = rand() % 9;
	int y = rand() % 9;

	std::pair<int, int> coords;
	coords.first = x;
	coords.second = y;

	return coords;
}


void Game::fileHandler(int newKills)	// handles the file i/o
{
	int previousKillTotal = 0;
	std::cout << "\n\nKILLS THIS GAME = " << newKills;

	std::ifstream myFileInput;	// this block handles reading in data from the file
	myFileInput.open("killCount.txt");
	myFileInput >> previousKillTotal;
	myFileInput.close();

	newKills += previousKillTotal;

	std::ofstream myFileOutput;	// this block is outputting to the file
	myFileOutput.open("killCount.txt");
	myFileOutput << newKills;
	myFileOutput.close();

	std::cout << "\nTOTAL KILLS = " << newKills << std::endl;
}

void Game::openingStory()
{
	std::cout << "In the world of Earthia, there is a small village of mages shrouded by mist.\n";
	std::cout << "The mages have been discriminated and treated like outcasts for years.\n";
	std::cout << "But with the rise in monster attacks recently, will the mages come to help?\n";
	std::cout << "The is where we meet our hero, Vivi. She is a young but talented mage that is travelling the \n";
	std::cout << "world in the hopes that someday the mages will be treated as equals\n";
	std::cout << "\nThe bird that would soar above the plain of tradition and\n";
	std::cout << "prejudice must have strong wings. \n";

	std::cout << "\nPress any key to continue.\n";
	std::cin >> m_advance;
	TheSoundManager::Instance()->playSound("selection", 1);
}

