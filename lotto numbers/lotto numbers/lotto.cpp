//includes
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>

using namespace std;

int main()
{
	vector<int> numbers;	//create vector to store numbers
	srand(static_cast<unsigned int>(time(0)));	//seed random function with current time

	for (int i = 0; i < 6; i++)	//for loop to add 6 numbers
	{
		
		int randomNumber = rand() % 49 + 1;	//get random number

		if (find(numbers.begin(), numbers.end(), randomNumber) != numbers.end() && (i != 0)) // Look through the vector for die
		{
			i--; // If found repeat loop
		}
		else
		{
			numbers.push_back(randomNumber); // else put in vector
		}

	}

	for (int i = 0; i < numbers.size(); i++)	//for loop to print random numbers
	{
		int printBoi;	// int to store a number
		printBoi = numbers[i];	//setting printBoi to the current position in the vector
		cout << printBoi << "\n";	// printing it out
	}

	getchar();	// makes the program wait for input 
	return 0;	// exit code
}