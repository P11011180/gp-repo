#ifndef CRITTER_H
#define CRITTER_H
#include <iostream>
using namespace std;

class Critter
{
public:          
    Critter(int hunger = 0, int boredom = 0, int m_age = 0); 
    void Talk();
    void Eat(int food = 4);
    void Play(int fun = 4);
	void Sleep();
	void Age();
private:
    int m_Hunger;
    int m_Boredom;
	int m_age;
    int GetMood() const;
    void PassTime(int time = 1);
	void getAge() const;

};


#endif


