
#include "Troll.h"

Troll::Troll(int health, int speed) :
	Creature(health, speed)

{}

void Troll::Greet() const
{
	cout << "The Troll shouts 'AAARRRRGGGHHHH'.\n";
}

void Troll::DisplayHealth() const
{
	cout << "The Trolls health is " << m_Health << endl;
}