
#include "Orc.h"
#include "Troll.h"
#include <vector>

int main()
{
	Creature* pCreature = new Orc();
	pCreature->Greet();
	pCreature->DisplayHealth();
	pCreature->DisplaySpeed();

	Creature* pCreature2 = new Troll();
	pCreature2->Greet();
	pCreature2->DisplayHealth();
	pCreature2->DisplaySpeed();

	cout << "\n\n\n";

	vector<Creature*> vpCreatures;	// create a vector that contains pointers to creatures
	vpCreatures.push_back(pCreature);	//push the creature pointers onto the vector
	vpCreatures.push_back(pCreature2);

	vector<Creature*>::iterator iter;	//an iterator to loop through the vector

	for (iter = vpCreatures.begin(); iter != vpCreatures.end(); iter++)	//forloop to search through the vector
	{
		//calling the functions on each creature in the vector
		(*iter)->Greet();
		(*iter)->DisplayHealth();
		(*iter)->DisplaySpeed();
	}



	getchar();
	return 0;
}
