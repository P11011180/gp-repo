#include "PlayState.h"
#include "TextureManager.h"
#include "Game.h"
#include "Player.h"
#include "Enemy.h"
#include "PauseState.h"
#include "GameOverState.h"

const std::string PlayState::s_playID = "PLAY";

void PlayState::update()
{
//		std::cout << "entering PlayState:update loop \n";

	if(TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_SPACE))
	{
		TheGame::Instance()->getStateMachine()->pushState(new PauseState());
	}

	for(int i = 0; i < m_gameObjects.size(); i++)
	{
		m_gameObjects[i]->update();
	}

	if(checkCollision(dynamic_cast<SDLGameObject*>(m_gameObjects[0]), dynamic_cast<SDLGameObject*>(m_gameObjects[1])) ||
		checkCollision(dynamic_cast<SDLGameObject*>(m_gameObjects[0]), dynamic_cast<SDLGameObject*>(m_gameObjects[2])))
	{
		m_gameObjects.front()->collision();
		m_gameObjects.front()->kill();
		//TheGame::Instance()->getStateMachine()->pushState(new GameOverState());

	}
}

void PlayState::render()
{
	for(int i = 0; i < m_gameObjects.size(); i++)
	{
		m_gameObjects[i]->draw();
	}

}

bool PlayState::onEnter()
{
	if(!TheTextureManager::Instance()->load("assets/helicopter.png","helicopter", TheGame::Instance()->getRenderer()))
	{
	return false;
	}
	if (!TheTextureManager::Instance()->load("assets/ghost.png", "ghost", TheGame::Instance()->getRenderer()))
	{
		return false;
	}
	if(!TheTextureManager::Instance()->load("assets/helicopter2.png","helicopter2", TheGame::Instance()->getRenderer()))
	{
	return false;
	}
	GameObject* player = new Player(new LoaderParams(250, 100, 128,55, "helicopter"));
	GameObject* enemy = new Enemy(new LoaderParams(100, 100, 128,55, "helicopter2"));
	GameObject* enemy2 = new Enemy(new LoaderParams(500, 100, 128, 55, "helicopter2"));
	GameObject* enemy3 = new Enemy(new LoaderParams(100, 100, 128, 55, "helicopter2"));
	GameObject* enemy4 = new Enemy(new LoaderParams(500, 100, 128, 55, "helicopter2"));
	GameObject* enemy5 = new Enemy(new LoaderParams(100, 100, 128, 55, "helicopter2"));
	GameObject* enemy6 = new Enemy(new LoaderParams(500, 100, 128, 55, "helicopter2"));
	GameObject* enemy7 = new Enemy(new LoaderParams(123, 100, 128, 55, "helicopter2"));
	GameObject* enemy8 = new Enemy(new LoaderParams(123, 154, 128, 55, "helicopter2"));
	GameObject* enemy9 = new Enemy(new LoaderParams(234, 167, 128, 55, "helicopter2"));
	GameObject* enemy10 = new Enemy(new LoaderParams(111, 123, 128, 55, "helicopter2"));
	GameObject* enemy11 = new Enemy(new LoaderParams(500, 123, 128, 55, "helicopter2"));

	m_gameObjects.push_back(player);
	m_gameObjects.push_back(enemy);
	m_gameObjects.push_back(enemy2);
	m_gameObjects.push_back(enemy3);
	m_gameObjects.push_back(enemy4);
	m_gameObjects.push_back(enemy5);
	m_gameObjects.push_back(enemy6);
	m_gameObjects.push_back(enemy7);
	m_gameObjects.push_back(enemy8);
	m_gameObjects.push_back(enemy9);
	m_gameObjects.push_back(enemy10);
	m_gameObjects.push_back(enemy11);

	for (long i = 0; i < 1000; i++)
	{
		GameObject* enemy = new Enemy(new LoaderParams(i*3, i*2, 128, 55, "helicopter2"));
		GameObject* enemy2 = new Enemy(new LoaderParams(i * 4, i * 3, 128, 55, "helicopter"));
		m_gameObjects.push_back(enemy);
		m_gameObjects.push_back(enemy2);
	}

	std::cout << "entering PlayState\n";
	return true;
}

bool PlayState::onExit()
{
	for(int i = 0; i < m_gameObjects.size(); i++)
	{
		m_gameObjects[i]->clean();
	}
	m_gameObjects.clear();
	TheTextureManager::Instance()->clearFromTextureMap("helicopter");
	std::cout << "exiting PlayState\n";

	return true;
}

bool PlayState::checkCollision(SDLGameObject* p1, SDLGameObject* p2)
{
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;

	leftA = p1->getPosition().getX();
	rightA = p1->getPosition().getX() + p1->getWidth();
	topA = p1->getPosition().getY();
	bottomA = p1->getPosition().getY() + p1->getHeight();

	//Calculate the sides of rect B
	leftB = p2->getPosition().getX();
	rightB = p2->getPosition().getX() + p2->getWidth();
	topB = p2->getPosition().getY();
	bottomB = p2->getPosition().getY() + p2->getHeight();

	//If any of the sides from A are outside of B
	if( bottomA <= topB ){return false;}
	if( topA >= bottomB ){return false; }
	if( rightA <= leftB ){return false; }
	if( leftA >= rightB ){return false;}
	return true;
}


