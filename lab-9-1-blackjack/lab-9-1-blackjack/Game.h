#ifndef GAME_H
#define GAME_H
#include "Deck.h"
#include "Player.h"
#include "House.h"

class Game
{
public:
	Game(const vector<string>& names);

	~Game();

	//plays the game of blackjack    
	void Play();

private:
	Deck m_Deck;
	House m_House;
	vector<Player> m_Players;
	int m_Pot;
};

#endif