#pragma once

#include <iostream>
using namespace std;

class Enemy
{
public:
	int m_Damage;

	Enemy();
	void Attack() const;
};