#ifndef SDL_Game_Programming_Book_Player_h
#define SDL_Game_Programming_Book_Player_h

#include <iostream>
#include "SDLGameObject.h"
#include "InputHandler.h"
#include "GameObjectFactory.h"


class Player : public SDLGameObject
{
public:
	//Player(const LoaderParams* pParams);
	
	Player ();
	
	virtual void draw();
	virtual void update();
	virtual void clean();

	// new load function
	virtual void load(const LoaderParams* pParams);

private:
	void handleInput(); //page 89

};

// for the factory
class PlayerCreator : public BaseCreator {
    GameObject* createGameObject() const { return new Player(); }
};

#endif