#pragma once

#include "Enemy.h"

class Boss : public Enemy
{
public:
	Boss();
	void SpecialAttack() const;

private:
	int m_DamageMultiplier;
};