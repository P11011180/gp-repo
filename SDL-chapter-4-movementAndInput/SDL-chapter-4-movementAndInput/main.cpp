#include "Game.h"
#include <Windows.h>
#include <stdio.h>
// our Game object
//Game* g_game = 0;
int main(int argc, char* argv[])
{
	AllocConsole();
	FILE* myFile;
	freopen_s(&myFile,"CON","w", stdout);
	// g_game = new Game();
	if (TheGame::Instance()->init("Chapter 1", 100, 100, 640, 480, false))
	{

		while(TheGame::Instance()->running())
		{
			TheGame::Instance()->handleEvents();
			TheGame::Instance()->update();
			TheGame::Instance()->render();
			SDL_Delay(10); // add the delay
		}
	
	}
	else
	{
		std::cout << "game init failure - " << SDL_GetError() << "\n";
		return -1;
	}
	TheGame::Instance()->clean();
	return 0;
}