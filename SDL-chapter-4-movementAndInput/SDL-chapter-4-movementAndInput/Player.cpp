#include <iostream>
#include "Player.h"
#include "Game.h"
Player::Player(const LoaderParams* pParams) :SDLGameObject(pParams)
{
	m_fuel = 600;
}


void Player::draw()
{
	SDLGameObject::draw(); // we now use SDLGameObject
	TheTextureManager::Instance()->draw("fuelbar", 32, 16, m_fuel, 16,TheGame::Instance()->getRenderer(), SDL_FLIP_NONE);
	if (m_fuel == 0)
	{
		TheTextureManager::Instance()->draw("uDed", 0, 0, 640, 480, TheGame::Instance()->getRenderer(), SDL_FLIP_NONE);

	}
}

void Player::update()
{


	m_currentFrame = int(((SDL_GetTicks() / 100) % 1));
  // m_acceleration.setX(-0.01); //page 77

	handleInput(); //every update we process the keyboard events detected by input handler
	//system("CLS");
	std::cout << "Fuel = " << m_fuel << std::endl;

	SDLGameObject::update(); // we call this so that m_velocity is uodated by acceleration (if any)
							// and the position is updated by the velocity value
						   //we could omit this and set position in this function 
}

void Player::clean()
{
}

//we only use the keyboard in this example, we do not check bounds
void Player::handleInput()
{
	if(TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_RIGHT))
	{
		m_velocity.setX(1);

	}
	else if(TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_LEFT))
	{
		m_velocity.setX(-1);

	}
	else if(TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_UP) && (m_fuel > 0))
	{
		m_velocity.setY(-1);
		m_fuel--;
	}
	else if((m_position.getY() < 400))
	{
		m_velocity.setX(0);
		m_velocity.setY(3);
	}
	else 
	{
		m_velocity.setX(0);
		m_velocity.setY(0);
	}
	
}




