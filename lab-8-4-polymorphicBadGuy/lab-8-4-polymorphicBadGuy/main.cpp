
#include "Boss.h"


int main()
{
	cout << "Calling Attack() on Boss object through pointer to Enemy:\n";
	Enemy* pBadGuy = new Boss();
	Enemy* pBudGay = new Boss();
	pBadGuy->Attack();
	pBudGay->Attack();
	cout << "\n\nDeleting pointer to Enemy:\n";
	delete pBadGuy;
	delete pBudGay;
	pBadGuy = 0;
	pBudGay = 0;
	getchar();
	return 'MEME';
}
