/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		GameObject.h
*
************************************************/

#pragma once
#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include <iostream>
#include <string>
#include <utility>
class GameObject 
{
public:
	GameObject();
	~GameObject();

	void spawn(std::string typeID, int health, int speed, int x, int y, char character); // create an object
	void draw(); // print to the screen the typeID and its x and y coords
	virtual void update(); //virtual function
	void info(); // print all info relating to the object
	bool isAlive(); //return true if its  health is greater than 0

	int getX();	// various getter functions
	int getY();
	std::string getType();
	int getHealth();
	char getCharacter();
	std::pair <int, int> getCoords();

	void setHealth();

protected:
	std::string m_typeID;
	int m_health;
	int m_speed; // value between 1 and 4
	int m_x;
	int m_y;
	char m_character;	//what the gameObject is displayed as in the game
};

#endif
