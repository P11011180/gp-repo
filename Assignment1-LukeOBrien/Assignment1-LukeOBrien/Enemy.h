/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		Enemy.h
*
************************************************/
#pragma once
#ifndef ENEMY_H
#define ENEMY_H

#include "GameObject.h"

class Enemy : public GameObject
{
public:
	Enemy();
	~Enemy();
	virtual void update();
	void spawn(std::string typeID, int health, int speed, int x, int y, char character, int patrol); 

protected:
	int m_patrolDirection;	//to store which direction the enemy will patrol in
};
#endif