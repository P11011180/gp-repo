/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		Game.cpp
*
************************************************/
#include "Game.h"

Game::Game()
{
	m_running = true;
	DisplayMap* map = new DisplayMap();
	m_enemyKillCount = 0;
}

Game::~Game()
{

}


void Game::init()
{
	Player* player = new Player();		//creating the player and pushing them onto the vector
	player->spawn("Player", 160, 1, getRandomCoords().first, getRandomCoords().second, 'O');
	vpGameObjs.push_back(player);

	//using a loop to create 3 enemies
	for (int i = 1; i < 4; i++)
	{
		Enemy* enemy = new Enemy();
		enemy->spawn("Enemy", 150, 1, getRandomCoords().first, getRandomCoords().second, 'X', i);
		vpGameObjs.push_back(enemy);
	}


}


void Game::draw() // cycle through the vector of Game Objects ( enemy and player objects) and call the draw function for each object
{
	std::vector <GameObject*>::const_iterator iter;
	for (iter = vpGameObjs.begin(); iter != vpGameObjs.end(); ++iter)
	{
		(*iter)->draw();
	}
}


void Game::update()	//cycle through the vector of Game Objects (enemy and player objects) and call the update function for each object
{
	std::vector <GameObject*>::const_iterator iter;	//defining a constant iterator

	map->createMap();	//creating the map

	for (iter = vpGameObjs.begin(); iter != vpGameObjs.end(); ++iter)	//iterates through the vector and places all gameObjects in the map
	{
		map->mapGameObjects((*iter)->getCoords(), (*iter)->getCharacter());
	}

	map->printMap();	//prints the map

	for (iter = vpGameObjs.begin(); iter != vpGameObjs.end(); ++iter)	// calls the update function of all gameObjects
	{
		(*iter)->update();
	}
	

}


void Game::battle()
{
	int playerX, playerY, playerHealth;	//player variables

	std::vector <GameObject*>::iterator iter;

	playerX = vpGameObjs.front()->getX();	//get the players coords
	playerY = vpGameObjs.front()->getY();
	playerHealth = vpGameObjs.front()->getHealth(); //get the players health

	for (iter = vpGameObjs.begin(); iter != vpGameObjs.end(); ++iter)
	{
		if ((*iter)->getType() == "Enemy" && (playerX == (*iter)->getX()) && (playerY == (*iter)->getY()))	//if an enemy is on the same position as the player
		{
			if (playerHealth >= (*iter)->getHealth())	//if the player has more health
			{
				std::cout << "Enemy Destroyed" << std::endl;
				(*iter)->setHealth();	//set the enemies health to zero
				m_enemyKillCount++;
			}
			else
			{
				std::cout << "oof" << std::endl;
				vpGameObjs.front()->setHealth();	//set the players health to zero
				m_running = false;	//end the game
			}
		}		
	}
}


void Game::info()	// call info() for each gameObject
{
	std::vector <GameObject*>::const_iterator iter;
	for (iter = vpGameObjs.begin(); iter != vpGameObjs.end(); ++iter)
	{
		(*iter)->info();
	}
}


void Game::clean()	//remove any gameObjects that have zero health
{
	std::vector <GameObject*>::iterator iter;

	for (iter = vpGameObjs.begin(); iter != vpGameObjs.end(); )
	{
		if ((!(*iter)->isAlive()))
		{
			delete (*iter);
			iter = vpGameObjs.erase(iter);
		}
		else
		{
			++iter;
		}
	}
}



int Game::getKillCount()	
{
	return m_enemyKillCount;
}


bool Game::getRunning()
{
	return m_running;
}


void Game::setRunning(bool running)
{
	m_running = running;
}


std::pair<int, int> Game::getRandomCoords() //generate random coords and return them as a pair
{
	
	int x = rand() % 9;
	int y = rand() % 9;

	std::pair<int, int> coords;
	coords.first = x;
	coords.second = y;

	return coords;
}


void Game::fileHandler(int newKills)	// handles the file i/o
{
	int previousKillTotal = 0;
	std::cout << "\n\nKILLS THIS GAME = " << newKills;

	std::ifstream myFileInput;	// this block handles reading in data from the file
	myFileInput.open("killCount.txt");
	myFileInput >> previousKillTotal;
	myFileInput.close();

	newKills += previousKillTotal;

	std::ofstream myFileOutput;	// this block is outputting to the file
	myFileOutput.open("killCount.txt");
	myFileOutput << newKills;
	myFileOutput.close();

	std::cout << "\nTOTAL KILLS = " << newKills << std::endl;
}


