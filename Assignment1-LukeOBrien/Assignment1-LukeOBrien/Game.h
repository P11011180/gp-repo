/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		Game.h
*
************************************************/
#pragma once
#ifndef GAME_H
#define GAME_H

#include "Player.h"
#include "Enemy.h"
#include <vector>
#include <algorithm>
#include <time.h>
#include <stdlib.h> 
#include "DisplayMap.h"
#include <iostream>
#include <fstream>
class Game
{
public:
	Game();
	~Game();

	void init();	//functions for the game loop
	void draw(); 
	void update();
	void battle();
	void info();
	void clean();

	int getKillCount();
	bool getRunning();
	void setRunning(bool running);
	std::pair<int, int> getRandomCoords();	//generates random coordinates
	void fileHandler(int newKills);			//deals with file i/o

protected:
	DisplayMap* map = new DisplayMap();	//pointer for displaying the map
	std::vector <GameObject*> vpGameObjs;	//vector for the game object pointers
	bool m_running;		// check to see if the game isnt over
	int m_enemyKillCount;	//how many enemies you have killed
};
#endif
