/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		GameObject.cpp
*
************************************************/
#include "GameObject.h"

GameObject::GameObject()
{
	std::cout << "GAME OBJECT CONSTRUCTOR CALLED" << std::endl;
}
GameObject::~GameObject()
{
	std::cout << "GAME OBJECT DECONSTRUCTOR CALLED" << std::endl;
}

void GameObject::spawn(std::string typeID, int health, int speed, int x, int y, char character)
{
	m_typeID = typeID;
	m_health = health;
	m_speed = speed;
	m_x = x;
	m_y = y;
	m_character = character;
}


void GameObject::draw()
{
	std::cout << "TypeID: " << m_typeID << "\t\t (x,y) coords = ( " << m_x << ", " << m_y << " )\n";
}


void GameObject::update()
{
	// its free real estate
}


void GameObject::info()
{
	std::cout << "TypeID = " << m_typeID;
	std::cout << "\tHealth = " << m_health;
	std::cout << "\tSpeed = " << m_speed;
	std::cout << "\tcoords = " << "(" << m_x << ", " << m_y << ")" << std::endl;
}


bool GameObject::isAlive()
{
	if (m_health > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

int GameObject::getX()	//just a bunch of getters 
{
	return m_x;
}

int GameObject::getY()
{
	return m_y;
}

std::string GameObject::getType()
{
	return m_typeID;
}

int GameObject::getHealth()
{
	return m_health;
}

void GameObject::setHealth()	// 'kills' gameObject
{
	m_health = 0;
}


char GameObject::getCharacter()
{
	return m_character;
}


std::pair<int, int> GameObject::getCoords()
{
	return std::make_pair(m_x, m_y);
}
