/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		Enemy.cpp
*
************************************************/
#include "Enemy.h"

Enemy::Enemy()
{
	std::cout << "ENEMY CONSTRUCTER CALLED\n";
}

Enemy::~Enemy()
{

	std::cout << "ENEMY DECONSTRUCTER CALLED\n";
}

void Enemy::update()
{
	m_health -= (m_speed * 3);	//health loss while moving

	if ((m_patrolDirection % 4) == 0) // when patroling UP
	{
		if ((m_y - m_speed) >= 0)
		{
			m_y -= m_speed;
		}
		else
		{
			m_y = 0;
			m_patrolDirection = 1; // bounce off game boundary and go DOWN
		}
	}

	if ((m_patrolDirection % 4) == 1) // when patroling DOWN
	{
		if ((m_y + m_speed) <= 9)
		{
			m_y += m_speed;
		}
		else
		{
			m_y = 9;
			m_patrolDirection = 0; // bounce off game boundary and go UP
		}
	}

	if ((m_patrolDirection % 4) == 2) // when patroling LEFT
	{
		if ((m_x - m_speed) >= 0)
		{
			m_x -= m_speed;
		}
		else
		{
			m_x = 0;
			m_patrolDirection = 3; // change direction to RIGHT
		}
	}

	if ((m_patrolDirection % 4) == 3) // when patroling RIGHT
	{
		if ((m_x + m_speed) <= 9)
		{
			m_x += m_speed;
		}
		else
		{
			m_x = 9;
			m_patrolDirection = 2; // change direction to LEFT
		}
	}
}


void Enemy::spawn(std::string typeID, int health, int speed, int x, int y, char character, int patrol)	// initialises all of the enemies variables
{
	m_typeID = typeID;
	m_health = health;
	m_speed = speed;
	m_x = x;
	m_y = y;
	m_character = character;
	m_patrolDirection = patrol;
}
