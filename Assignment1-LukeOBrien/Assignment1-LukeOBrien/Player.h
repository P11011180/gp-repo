/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		Player.h
*
************************************************/



#pragma once
#ifndef PLAYER_H //defining player
#define PLAYER_H

#include "GameObject.h"

class Player : public GameObject // player is a gameobject
{
public:
	Player();	
	~Player();
	virtual void update();
	void movePlayer();	//function to deal with player movement

protected:
	std::string m_moveDirection;	//to store the user selected move direction

};
#endif

