/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		Player.cpp
*
************************************************/
#include "Player.h"

Player::Player()
{
	std::cout << "PLAYER CONSTRUCTER CALLED\n";
}

Player::~Player()
{
	std::cout << "PLAYER DECONSTRUCTER CALLED\n";
}

void Player::update()
{
	std::cout << "Please enter which direction you want to move in: \n";	// ask the user to select a direction
	std::cout << "('w' = UP) ('a' = LEFT) ('s' = DOWN) ('d' = RIGHT) \n";
	std::cin >> m_moveDirection;

	if (!(m_moveDirection == "w" || m_moveDirection == "a" || m_moveDirection == "s" || m_moveDirection == "d"))	//check for valid input. if not valid then the player misses a turn
	{
		std::cout << "You have entered an incorrect input and have lost your turn\n";
	}
	else
	{
		movePlayer();
	}
	m_health -= (m_speed * 2);	//movement health drain
}


void Player::movePlayer() // function to move the player
{
	if (m_moveDirection == "w") // performs boundary checks for moving UP
	{
		if ((m_y - m_speed) >= 0)	//if the player doesnt collide with the wall
		{
			m_y -= m_speed;	// move player
		}
		else
		{
			m_y = 0;	//hit wall
		}
	}
	

	if (m_moveDirection == "a")// performs boundary checks for moving LEFT
	{
		if ((m_x - m_speed) >= 0)
		{
			m_x -= m_speed;
		}
		else
		{
			m_x = 0;
		}
	}
	
	if (m_moveDirection == "s") // performs boundary checks for moving DOWN
	{
		if ((m_y + m_speed) <= 9)
		{
			m_y += m_speed;
		}
		else
		{
			m_y = 9;
		}
	}
	
	if (m_moveDirection == "d") // performs boundary checks for moving RIGHT
	{
		if ((m_x + m_speed) <= 9)
		{
			m_x += m_speed;
		}
		else
		{
			m_y = 9;
		}
	}
	
}
