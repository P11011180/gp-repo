// a program, a game, a programmed game
/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		main.cpp
*
************************************************/

#include "Game.h"
int main()
{
	srand(time(NULL));	//to seed the random coords
	int count = 0;

	Game* game = new Game();	// game pointer
	game->init();	// initial setup of the game

	while ((game->getRunning() == true) && (count < 20))	//while player is still alive and <20 turns have been played
	{
		game->draw();
		game->update();
		game->battle();
		game->info();
		game->clean();
		count++;
	}

	game->info();

	if (count < 20)
	{
		std::cout << "\noof, the player has died!\n";
	}
	else
	{
		std::cout << "\The game is over!" << std::endl;
	}

	game->fileHandler(game->getKillCount());	//calls the file handler

	system("pause");
	return 0;
}
