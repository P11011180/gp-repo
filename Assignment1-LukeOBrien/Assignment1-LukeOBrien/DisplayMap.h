/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		DisplayMap.h
*
************************************************/
#pragma once
#ifndef DISPLAYMAP_H
#define DISPLAYMAP_H

#define MAP_SIZE 10	// defining a constant for the map size
#include <iostream>

class DisplayMap
{
public:
	DisplayMap();
	~DisplayMap();

	void createMap();	//creates the map and fills with empty spaces
	void printMap();	//prints the map to the console
	void mapGameObjects(std::pair<int, int> coords, char character);	//puts the game objects into the map based on their coords

protected:
	char map[MAP_SIZE][MAP_SIZE];	//2D array for the map
	
};
#endif