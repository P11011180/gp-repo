/**********************************************
*
*	Name:			Luke O Brien
*
*	Student Number: P11011180
*
*	File Name:		DisplayMap.cpp
*
************************************************/
#include "DisplayMap.h"

DisplayMap::DisplayMap()
{
	std::cout << "MAP CONSTRUCTOR CALLED" << std::endl;
}

DisplayMap::~DisplayMap()
{
	std::cout << "MAP DECONSTRUCTOR CALLED" << std::endl;
}


void DisplayMap::createMap()	//creates and iterates through a 2D array while filling it with spaces
{
	for (int i = 0; i<MAP_SIZE; i++)
	{
		for (int j = 0; j<MAP_SIZE; j++)
		{
			map[i][j] = ' ';
		}
	}
}


void DisplayMap::printMap()	//prints the map to the screen
{
	std::cout << "\n\n	  x 0 1 2 3 4 5 6 7 8 9\n";
	std::cout << "	  ______________________" << std::endl;

	for (int i = 0; i < MAP_SIZE; i++) 
	{
		std::cout << "	" << i << " |";

		for (int j = 0; j < MAP_SIZE; j++) 
		{
			std::cout << " " << map[j][i]; 

		}
		std::cout << "|" << std::endl;
	}
	std::cout << "	y ----------------------\n";
	
}


void DisplayMap::mapGameObjects(std::pair<int, int> coords, char character)	//uses the game objects coords to place them into the game
{
	int x = coords.first;
	int y = coords.second;
	map[x][y] = character;
}
