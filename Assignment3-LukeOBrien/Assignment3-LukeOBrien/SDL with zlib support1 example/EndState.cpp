//
//  MenuState.cpp
//  SDL Game Programming Book
//
//  Created by shaun mitchell on 09/02/2013.
//  Copyright (c) 2013 shaun mitchell. All rights reserved.
//
#include <iostream>
#include "EndState.h"
#include "TextureManager.h"
#include "Game.h"
#include "MenuButton.h"
#include "PlayState.h"
#include "InputHandler.h"
#include "StateParser.h"
#include <assert.h>

const std::string EndState::s_endID = "END";

// Callbacks
void EndState::s_menuToPlay()
{
	TheGame::Instance()->getStateMachine()->changeState(new PlayState());
}

void EndState::s_exitFromMenu()
{
	TheGame::Instance()->quit();
}

// end callbacks

void EndState::update()
{
	if (TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_SPACE))
	{
		s_menuToPlay();
	}
	if (!m_gameObjects.empty())
	{
		for (int i = 0; i < m_gameObjects.size(); i++)
		{
			if (m_gameObjects[i] != 0)
			{
				m_gameObjects[i]->update();
			}
		}
	}
}

void EndState::render()
{
	if (m_loadingComplete && !m_gameObjects.empty())
	{
		//TheTextureManager::Instance()->drawFrame("title", 50, 50, 512, 64, 0, 0, TheGame::Instance()->getRenderer(), 0.0, 255);

		for (int i = 0; i < m_gameObjects.size(); i++)
		{
			m_gameObjects[i]->draw();
		}
		printFinalScore();
	}
}

bool EndState::onEnter()
{
	// parse the state
	StateParser stateParser;
	stateParser.parseState("assets/attack.xml", s_endID, &m_gameObjects, &m_textureIDList);
	//TheTextureManager::Instance()->load("assets/title.png", "title", TheGame::Instance()->getRenderer());

	m_callbacks.push_back(0);
	m_callbacks.push_back(s_menuToPlay);
	m_callbacks.push_back(s_exitFromMenu);

	// set the callbacks for menu items
	setCallbacks(m_callbacks);

	m_loadingComplete = true;
	std::cout << "entering EndState\n";
	return true;
}

bool EndState::onExit()
{
	m_exiting = true;

	// clean the game objects
	if (m_loadingComplete && !m_gameObjects.empty())
	{
		m_gameObjects.back()->clean();
		m_gameObjects.pop_back();
	}

	m_gameObjects.clear();


	/* clear the texture manager
	for(int i = 0; i < m_textureIDList.size(); i++)
	{
	TheTextureManager::Instance()->clearFromTextureMap(m_textureIDList[i]);
	}
	*/

	// reset the input handler
	TheInputHandler::Instance()->reset();

	std::cout << "exiting EndState\n";
	return true;
}

void EndState::printFinalScore()
{
	//creating font with  arguements (font file, font size)
	TTF_Font * font = TTF_OpenFont("arial.ttf", 20);
	const char * error = TTF_GetError();

	//creating a string stream	
	std::stringstream strm;
	strm << "Your Score was: " << TheGame::Instance()->getPlayerScore();
	//std::cout << strm.str() << std::endl;

	//creating a colour
	SDL_Color colour = { 255, 255, 255 };

	//creating a surface for the font using the font and colour defined above
	SDL_Surface * surface = TTF_RenderText_Solid(font, strm.str().c_str(), colour);

	//creating a texture using the renderer and surface with the text
	SDL_Texture * texture = SDL_CreateTextureFromSurface(TheGame::Instance()->getRenderer(), surface);

	int texW = 0;
	int texH = 0;
	SDL_QueryTexture(texture, NULL, NULL, &texW, &texH);
	SDL_Rect dstrect = { 150, 32, texW , texH };

	SDL_RenderCopy(TheGame::Instance()->getRenderer(), texture, NULL, &dstrect);
	SDL_RenderPresent(TheGame::Instance()->getRenderer());

	SDL_DestroyTexture(texture);
	SDL_FreeSurface(surface);
	TTF_CloseFont(font);
}

void EndState::setCallbacks(const std::vector<Callback>& callbacks)
{
	// go through the game objects
	if (!m_gameObjects.empty())
	{
		for (int i = 0; i < m_gameObjects.size(); i++)
		{
			// if they are of type MenuButton then assign a callback based on the id passed in from the file
			if (dynamic_cast<MenuButton*>(m_gameObjects[i]))
			{
				MenuButton* pButton = dynamic_cast<MenuButton*>(m_gameObjects[i]);
				pButton->setCallback(callbacks[pButton->getCallbackID()]);
			}
		}
	}
}

