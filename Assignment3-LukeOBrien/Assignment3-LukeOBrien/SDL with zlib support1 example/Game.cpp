//
//  Game.cpp
//  SDL Game Programming Book
//
//
#include "Game.h"
#include "TextureManager.h"
#include "InputHandler.h"
#include "MainMenuState.h"
#include "RoomTransitionState.h"
#include "EndState.h"
#include "GameObjectFactory.h"
#include "MenuButton.h"
#include "AnimatedGraphic.h"
#include "Player.h"
#include "ScrollingBackground.h"
#include "SoundManager.h"
#include "RoofTurret.h"
#include "ShotGlider.h"
#include "ShotGliderSon.h"
#include "FuelPickup.h"
#include "Eskeletor.h"
#include "Level1Boss.h"
#include "GameOverState.h"
#include "BigGunItem.h"
#include "CrazyBullets.h"
#include <iostream>
#include <SDL.h>
#include <SDL_ttf.h>
#include <string>
#include <sstream> 

using namespace std;

Game* Game::s_pInstance = 0;


Game::Game():
m_pWindow(0),
m_pRenderer(0),
m_bRunning(false),
m_pGameStateMachine(0),
m_playerLives(5),
m_scrollSpeed(1.6),
m_bLevelComplete(false),
m_bChangingState(false),
m_playerScore(0),
m_playerFuel(1)
{
    // add some level files to an array
    m_levelFiles.push_back("assets/map1.tmx");
	m_levelFiles.push_back("assets/map2.tmx");
    
    // start at this level
    m_currentLevel = 1;
}

Game::~Game()
{
    // we must clean up after ourselves to prevent memory leaks
    m_pRenderer= 0;
    m_pWindow = 0;
}


bool Game::init(const char* title, int xpos, int ypos, int width, int height, bool fullscreen)
{
    int flags = 0;
    
    // store the game width and height
    m_gameWidth = width;
    m_gameHeight = height;
    
    if(fullscreen)
    {
        flags = SDL_WINDOW_FULLSCREEN;
    }
    
    // attempt to initialise SDL
    if(SDL_Init(SDL_INIT_EVERYTHING) == 0)
    {
        cout << "SDL init success\n";
        // init the window
        m_pWindow = SDL_CreateWindow(title, xpos, ypos, width, height, flags);
        
        if(m_pWindow != 0) // window init success
        {
            cout << "window creation success\n";
            m_pRenderer = SDL_CreateRenderer(m_pWindow, -1, SDL_RENDERER_ACCELERATED);
            
            if(m_pRenderer != 0) // renderer init success
            {
                cout << "renderer creation success\n";
                SDL_SetRenderDrawColor(m_pRenderer, 0,0,0,255);
            }
            else
            {
                cout << "renderer init fail\n";
                return false; // renderer init fail
            }
        }
        else
        {
            cout << "window init fail\n";
            return false; // window init fail
        }
    }
    else
    {
        cout << "SDL init fail\n";
        return false; // SDL init fail
    }
    
    // add some sound effects - TODO move to better place
    TheSoundManager::Instance()->load("assets/DST_ElectroRock.ogg", "music1", SOUND_MUSIC);
    TheSoundManager::Instance()->load("assets/boom.wav", "explode", SOUND_SFX);
    TheSoundManager::Instance()->load("assets/phaser.wav", "shoot", SOUND_SFX);
    
    TheSoundManager::Instance()->playMusic("music1", -1);
    
    //TheInputHandler::Instance()->initialiseJoysticks();
    
    // register the types for the game
    TheGameObjectFactory::Instance()->registerType("MenuButton", new MenuButtonCreator());
    TheGameObjectFactory::Instance()->registerType("Player", new PlayerCreator());
    TheGameObjectFactory::Instance()->registerType("AnimatedGraphic", new AnimatedGraphicCreator());
    TheGameObjectFactory::Instance()->registerType("ScrollingBackground", new ScrollingBackgroundCreator());
    TheGameObjectFactory::Instance()->registerType("Turret", new TurretCreator());
    TheGameObjectFactory::Instance()->registerType("Glider", new GliderCreator());
    TheGameObjectFactory::Instance()->registerType("ShotGlider", new ShotGliderCreator());
	TheGameObjectFactory::Instance()->registerType("ShotGliderSon", new ShotGliderSonCreator());
	TheGameObjectFactory::Instance()->registerType("Pickup", new PickupCreator());
	TheGameObjectFactory::Instance()->registerType("BigGunItem", new BigGunItemCreator());
	TheGameObjectFactory::Instance()->registerType("CrazyBullets", new CrazyBulletsCreator());
	TheGameObjectFactory::Instance()->registerType("RoofTurret", new RoofTurretCreator());
    TheGameObjectFactory::Instance()->registerType("Eskeletor", new EskeletorCreator());
    TheGameObjectFactory::Instance()->registerType("Level1Boss", new Level1BossCreator());
    
    // start the menu state
    m_pGameStateMachine = new GameStateMachine();
    m_pGameStateMachine->changeState(new MainMenuState());

	// initializing SDL text
	TTF_Init();
    
    m_bRunning = true; // everything inited successfully, start the main loop
    return true;
}

void Game::setCurrentLevel(int currentLevel)
{
    m_currentLevel = currentLevel;
	if (m_currentLevel != m_levelFiles.size() + 1)
	 {
		m_pGameStateMachine->changeState(new RoomTransitionState());
	}
	else
	{
		m_pGameStateMachine->changeState(new EndState());
		m_currentLevel = 1;
	}
}

int Game::getPlayerScore()
{
	return m_playerScore;
}

void Game::addPlayerScore(int score)
{
	m_playerScore += score;
}

void Game::printScore()
{
	//creating font with  arguements (font file, font size)
	TTF_Font * font = TTF_OpenFont("arial.ttf", 20);
	const char * error = TTF_GetError();

	//creating a string stream	
	std::stringstream strm;
	strm << "Score: " << m_playerScore;
	//std::cout << strm.str() << std::endl;

	//creating a colour
	SDL_Color colour = { 255, 255, 255 };

	//creating a surface for the font using the font and colour defined above
	SDL_Surface * surface = TTF_RenderText_Solid(font, strm.str().c_str(), colour);

	//creating a texture using the renderer and surface with the text
	SDL_Texture * texture = SDL_CreateTextureFromSurface(m_pRenderer, surface);

	int texW = 0;
	int texH = 0;
	SDL_QueryTexture(texture, NULL, NULL, &texW, &texH);
	SDL_Rect dstrect = { 10, 32, texW , texH  };

	SDL_RenderCopy(m_pRenderer, texture, NULL, &dstrect);
	SDL_RenderPresent(m_pRenderer);

	SDL_DestroyTexture(texture);
	SDL_FreeSurface(surface);
	TTF_CloseFont(font);
}


int Game::getPlayerFuel()
{
	return m_playerFuel;
}


void Game::setPlayerFuel(int change)
{
	m_playerFuel += change;
}


void Game::printFuel()
{
	//creating font with  arguements (font file, font size)
	TTF_Font * font = TTF_OpenFont("arial.ttf", 20);
	const char * error = TTF_GetError();

	//creating a string stream	
	std::stringstream strm;
	strm << "Fuel: " << m_playerFuel;
	//std::cout << strm.str() << std::endl;

	//creating a colour
	SDL_Color colour;
	if (m_playerFuel > 100)
	{
		colour = { 255, 255, 255 };
	}
	else
	{
		colour = { 255, 20, 20 };
	}
	

	//creating a surface for the font using the font and colour defined above
	SDL_Surface * surface = TTF_RenderText_Solid(font, strm.str().c_str(), colour);

	//creating a texture using the renderer and surface with the text
	SDL_Texture * texture = SDL_CreateTextureFromSurface(m_pRenderer, surface);

	int texW = 0;
	int texH = 0;
	SDL_QueryTexture(texture, NULL, NULL, &texW, &texH);
	SDL_Rect dstrect = { 10, 72, texW , texH  };

	SDL_RenderCopy(m_pRenderer, texture, NULL, &dstrect);
	SDL_RenderPresent(m_pRenderer);

	SDL_DestroyTexture(texture);
	SDL_FreeSurface(surface);
	TTF_CloseFont(font);
}

void Game::addToInventory(std::string item)
{
	m_playerInventory.push_back(item);
}

bool Game::checkInventory(std::string item)
{
	if (std::find(m_playerInventory.begin(), m_playerInventory.end(), item) != m_playerInventory.end()) 
	{
		return true;
	}
	else 
	{
		return false;
	}
}


void Game::render()
{
    SDL_RenderClear(m_pRenderer);
    

	m_pGameStateMachine->render();


    SDL_RenderPresent(m_pRenderer);
}

void Game::update()
{
	if (m_playerFuel == 0)
	{
		m_pGameStateMachine->changeState(new EndState());
	}
		m_pGameStateMachine->update();

}

void Game::handleEvents()
{

		TheInputHandler::Instance()->update();

}

void Game::clean()
{
    cout << "cleaning game\n";
    
    TheInputHandler::Instance()->clean();
    
    m_pGameStateMachine->clean();
    
    m_pGameStateMachine = 0;
    delete m_pGameStateMachine;
    
    TheTextureManager::Instance()->clearTextureMap();
    
    SDL_DestroyWindow(m_pWindow);
    SDL_DestroyRenderer(m_pRenderer);
    SDL_Quit();
}


