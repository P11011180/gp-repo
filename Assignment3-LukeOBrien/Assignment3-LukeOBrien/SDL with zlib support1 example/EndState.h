#ifndef endState
#define endState

#include <vector>
#include "MenuState.h"
#include "GameObject.h"

class EndState : public MenuState
{
public:

	virtual ~EndState() {}

	virtual void update();
	virtual void render();

	virtual bool onEnter();
	virtual bool onExit();

	virtual std::string getStateID() const { return s_endID; }
	void printFinalScore();
private:

	virtual void setCallbacks(const std::vector<Callback>& callbacks);

	// call back functions for menu items
	static void s_menuToPlay();
	static void s_exitFromMenu();

	static const std::string s_endID;

	std::vector<GameObject*> m_gameObjects;
};

#endif /* defined(__SDL_Game_Programming_Book__MenuState__) */