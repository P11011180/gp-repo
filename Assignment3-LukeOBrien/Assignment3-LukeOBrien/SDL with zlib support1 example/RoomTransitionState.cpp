#include "RoomTransitionState.h"

const std::string RoomTransitionState::s_transitionID = "ROOM_TRANSITION";

void RoomTransitionState::update()
{
	if (m_loadingComplete && !m_exiting)
	{
		TheGame::Instance()->getStateMachine()->changeState(new PlayState());

	}
}

void RoomTransitionState::render()
{

}

bool RoomTransitionState::onEnter()
{

	m_loadingComplete = true;
	std::cout << "Entering RoomtransitionState\n";
	return true;
}

bool RoomTransitionState::onExit()
{
	m_exiting = true;

	std::cout << "Exiting RoomtransitionState\n";
	TheGame::Instance()->setLevelComplete(false);
	return true;
}

void RoomTransitionState::setCallbacks(const std::vector<Callback>& callbacks)
{
}
