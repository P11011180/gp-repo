//
//  Pickup.h
//  SDL Game Programming Book
//
//  Created by shaun mitchell on 30/03/2013.
//  Copyright (c) 2013 shaun mitchell. All rights reserved.
//

#ifndef __SDL_Game_Programming_Book__CrazyBullets__
#define __SDL_Game_Programming_Book__CrazyBullets__

#include <iostream>
#include "GameObjectFactory.h"
#include "BulletHandler.h"
#include "SoundManager.h"
#include <math.h>
#include "Enemy.h"

class CrazyBullets : public Enemy
{
public:

	CrazyBullets() : Enemy()
	{
		m_dyingTime = 50;
		m_health = 1;
		m_moveSpeed = 2;
		m_gap = 60;
	}

	virtual ~CrazyBullets() {}

	virtual void load(std::unique_ptr<LoaderParams> const &pParams)
	{
		ShooterObject::load(std::move(pParams));

		m_velocity.setX(-m_moveSpeed);
		m_velocity.setY(m_moveSpeed / 2);

		m_maxHeight = m_position.getY() + m_gap;
		m_minHeight = m_position.getY() - m_gap;
	}

	virtual void collision()
	{
		m_health -= 1;
		//std::cout << "coliding" << std::endl;
		if (m_health == 0)
		{
			if (!m_bPlayedDeathSound)
			{
				TheSoundManager::Instance()->playSound("explode", 0);

				m_textureID = "rewardCrazyBullets";
				m_currentFrame = 0;
				m_numFrames = 1;
				m_width = 38;
				m_height = 34;
				m_bDying = true;
				TheGame::Instance()->addToInventory("CrazyBullets");
				TheGame::Instance()->addPlayerScore(100);

			}
		}
	}

	virtual void update()
	{
		if (!m_bDying)
		{
			if (m_position.getY() >= m_maxHeight)
			{
				m_velocity.setY(-m_moveSpeed);
			}
			else if (m_position.getY() <= m_minHeight)
			{
				m_velocity.setY(m_moveSpeed);
			}
		}
		else
		{
			m_velocity.setX(0);
			m_velocity.setY(0);
			doDyingAnimation();
		}

		ShooterObject::update();

	}

	virtual std::string type() { return "CrazyBullets"; }

private:

	int m_maxHeight;
	int m_minHeight;
	int m_gap;
};

class CrazyBulletsCreator : public BaseCreator
{
	GameObject* createGameObject() const
	{
		return new CrazyBullets();
	}
};



#endif /* defined(__SDL_Game_Programming_Book__Glider__) */
